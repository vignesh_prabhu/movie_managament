﻿using Movie_Management.Service.MovieService;
using System;
using System.IO;
using System.Web.Mvc;

namespace Movie_Management.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movie)
        {
            _movieService = movie;
        }

        //[HttpGet]
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult CreateMovie()
        //{
        //    return View();
        //}

        [HttpPost]
        public JsonResult SaveUploadImage()
        {
            var message = string.Empty;
            var flag = false;
            if (Request.Files == null) return new JsonResult {Data = new {Message = message, Status = false}};
            var file = Request.Files[0];
            if (file == null) return new JsonResult {Data = new {Message = message, Status = false}};
            var fileName = file.FileName;

            try
            {
                file.SaveAs(Path.Combine(Server.MapPath("~/UploadedImages/" + fileName)));
                message = "File uploaded";
                flag = true;
            }
            catch (Exception)
            {
                message = "File upload failed! Please try again";
            }

            return new JsonResult { Data = new { Message = message, Status = flag } };
        }

        [HttpPost]
        public JsonResult SaveMovie(string ShowName,string ShowYearOfRelease,string ShowPlot,string ImagePath,long EntertainmentId,long[] Cast)
        {
            var result =_movieService.SaveMovie(ShowName, ShowYearOfRelease, ShowPlot, ImagePath, EntertainmentId,Cast);

            var message = result ? "Success" : "Failed";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult GetAllMovies()
        {
            var result = _movieService.GetAllMovie();
            if(result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                const string message = "Failed";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        //[HttpGet]
        //public ActionResult Edit()
        //{
        //    return View();
        //}

        [HttpGet]
        public JsonResult EditMovie(long id)
        {
            var result = _movieService.EditMovie(id);

            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                const string message = "Failed";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult EditMovieDetails(long showId, string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId, long[] Cast)
        {
            var result = _movieService.EditMovieDetails(showId, ShowName, ShowYearOfRelease, ShowPlot, ImagePath, EntertainmentId, Cast);

            var message = result ? "Success" : "Failed";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}