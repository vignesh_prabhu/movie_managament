﻿using Movie_Management.Service.ActorService;
using System;
using System.Web.Mvc;

namespace Movie_Management.Controllers
{
    public class ActorController : Controller
    {
        private readonly IActorsService _actors;

        public ActorController(IActorsService actors)
        {
            _actors = actors;
        }

        //[HttpGet]
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        public JsonResult GetActors()   
        {
            var result = _actors.GetActors();
            
            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                const string message = "Failed";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult SaveActor(long? ActorId,string ActorName,string ActorSex,DateTime ActorDob,string ActorBio)
        {
            var result = _actors.SaveActor(ActorId, ActorName, ActorSex, ActorDob, ActorBio);

            if (result!=null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                const string message = "Failed to insert! Please try again";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult DeleteActor(long id)
        {
            var result = _actors.DeleteActor(id);

            var message = result ? "Success" : "Failed to Delete! Please try again";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult EditActor(long id)
        {
            var result = _actors.EditActor(id);
            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                const string message = "Failed";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

    }
}