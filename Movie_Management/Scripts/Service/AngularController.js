﻿/// <reference path="../angular.js" />
/// <reference path="../angular.min.js" />      
/// <reference path="../angular-animate.js" />      
/// <reference path="../angular-animate.min.js" /> 
/// <reference path= "../angular-focusmanager.js"/>
/// <reference path="Module.js" />      
/// <reference path="FunctionService.js" />

app.controller('movieController', ['$scope', '$http', '$timeout', '$rootScope', '$window', 'movieService', function ($scope, $http, $timeout, $rootScope, $window, movieService) {

    //Default Variables
    $scope.submitText = "Save";
    $scope.submitted = false;
    $scope.message = "";
    $scope.isFormValid = false;

    //Form Validations
    $scope.$watch('f1.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });

    //time out function
    $scope.Timeout = function () {
        $scope.bool = true;
        $timeout(function () {
            $scope.bool = false;
        }, 2000);
    }

    //GetActors
    $scope.Actors = [];
    $scope.getActors = function () {
        $scope.TableRecordMessage = "";
        movieService.getActors()
            .then((resp) => {
                if (resp.data.length == 0) {
                    $scope.TableRecordMessage = "No Record Found";
                }
                else {
                    $scope.Actors = resp.data;
                    $scope.Actors.forEach(e => e.ActorDOB = new Date(parseInt(e.ActorDOB.substr(6))));
                }
            }, (err) => {
                err.message;
            });

    }
    $scope.getActors();

    // Variables   
    $scope.SelectedFileForUpload = null;
    $scope.IsFileValid = false;
    $scope.FileInvalidMessage = "";

    //File Validation   
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        $scope.message = "";
        if ($scope.SelectedFileForUpload != null) {
            if ((file.type == 'image/png' || file.type == 'image/jpeg' || file.type == 'image/gif') && file.size <= (1024 * 1024)) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Only JPEG/PNG/Gif Image can be upload )";
            }
        }
        else {
            $scope.FileInvalidMessage = "Image required!";
        }
        $scope.IsFileValid = isValid;
    };

    //File Select event    
    $scope.selectFileforUpload = function (file) {
        console.log(file);
        var files = file[0];
        $scope.poster = files.name;
        $scope.SelectedFileForUpload = file[0];
    }

    //Get List of Actors
    $scope.lst = [];
    $scope.change = function (a, cast) {
        if (cast)
            $scope.lst.push(a.ActorId);
        else
            $scope.lst.splice($scope.lst.indexOf(a), 1);
    };

    //Add Movie
    $scope.saveMovie = function () {
        $scope.submitted = true;
        $scope.message = "";
        $scope.ChechFileValid($scope.SelectedFileForUpload);
        //console.log($scope.isFormValid);
        if ($scope.IsFileValid) {
            movieService.UploadFile($scope.SelectedFileForUpload).then(function (resp) {
                movieService.saveMovie({ ShowName: $scope.movieName, ShowYearOfRelease: $scope.yearOfRelease, ShowPlot: $scope.plot, ImagePath: $scope.poster, EntertainmentId: 1, Cast: $scope.lst })
                    .then(function (resp) {
                        if (resp.data == 'Success') {
                            $scope.resetForm();
                            $scope.message = 'Inserted a new movie.';
                            $scope.Timeout();
                        }
                        else {
                            $scope.message = 'Failed to insert a movie';
                            $scope.Timeout();
                        }
                    });
            }, function (e) {
                alert(e);
            });
        }
        //selse {
        // $scope.message = "Please fill the required fields";
        // $scope.Timeout();
        //}  
    }

    //Clear Movie form
    $scope.resetForm = function () {
        $scope.movieName = "";
        $scope.yearOfRelease = "";
        $scope.plot = "";
        $scope.f1.$setPristine();
        $scope.submitted = false;
    }

    //Get All Movies
    $scope.movies = [];
    $scope.getAllMovies = function () {
        $scope.message = "";
        movieService.getAllMovies()
            .then((resp) => {
                if (resp.data.length == 0) {
                    $scope.message = "No Record Found";
                }
                else {
                    $scope.movies = resp.data;
                }
            }, (err) => {
                err.message;
            });
    }
    $scope.getAllMovies();


    //Save Actor
    $scope.saveActor = function () {
        if ($scope.submitText == 'Save') {
            $scope.submitted = true;
            $scope.message = "";

            if ($scope.isFormValid) {
                $scope.submitText = 'Please wait...';
                movieService.saveActor({ ActorName: $scope.actorName, ActorSex: $scope.gender, ActorDOB: $scope.dob, ActorBio: $scope.bio })
                    .then(function (resp) {
                        $scope.$applyAsync($scope.getActors());
                        if (resp.data == 'Success') {
                            $scope.message = "Inserted Actor Successfull."
                            $scope.Timeout();
                            $scope.submitText = 'Save';
                            ClearForm();
                        }
                        else {
                            $scope.message = "Failed to insert, Try again !";
                            $scope.Timeout();
                        }
                    });
            }
            else {
                $scope.message = "Please fill required fields value";
                $scope.Timeout();
            }
        }
    }

    //Clear Actor form
    function ClearForm() {
        $scope.actorName = "";
        $scope.gender = false;
        $scope.dob = "";
        $scope.bio = "";
        $scope.f1.$setPristine();
        $scope.submitted = false;
    }



    //delete Actor
    $scope.deleteActorDetail = function (index, id) {
        $scope.message = "";
        movieService.deleteActor(id)
            .then(function (response) {
                if (response.data == 'Success') {
                    $scope.Actors.splice(index, 1);
                    $scope.message = 'Deleted Actor!';
                    $scope.Timeout();
                }
                else {
                    $scope.message = 'Failed to delete, Try again!'
                    $scope.Timeout();
                }
            });
    }

    //Edit Movie
    $scope.EditMovie = [];
    $scope.editMovie = function (id) {
        $scope.message = "";
        movieService.editMovie(id)
            .then((resp) => {
                $scope.EditMovie = resp.data; 
                
                
            }, (err) => {
                $scope.message = err.message;
            });
    }
    console.log($scope.EditMovie);
   

}])



