﻿/// <reference path="../angular.js" />    
/// <reference path="../angular.min.js" />    
/// <reference path="../angular-animate.js" />    
/// <reference path="../angular-animate.min.js" /> 
/// <reference path="Module.js" />

app.factory('movieService', function ($http,$q) {

    return {
        getActors: function () {
            return $http.get('/Actor/GetActors');
        },

        UploadFile : function (file) {
            var formData = new FormData();
            formData.append("file", file);

            var defer = $q.defer();
            $http.post("/Movie/SaveUploadImage", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                })
                .then(function (d) {
                    defer.resolve(d);
                },
                function (error) {
                    defer.reject("File Upload Failed!");
                });
            return defer.promise;
        },

        saveMovie: function (obj) {
            return $http.post('/Movie/SaveMovie', obj);
        },

        getAllMovies: function () {
            return $http.get('/Movie/GetAllMovies');
        },

        saveActor: function (obj) {
            return $http.post('/Actor/SaveActor', obj);
        },

        deleteActor: function (id) {
            return $http.post('/Actor/DeleteActor/'+id);
        },

        editMovie: function (id) {
            return $http.get('/Movie/EditMovie/' + id);
        }
    }

});