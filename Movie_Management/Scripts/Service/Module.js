﻿/// <reference path="../angular.js" />    
/// <reference path="../angular.min.js" />    
/// <reference path="../angular-animate.js" />    
/// <reference path="../angular-animate.min.js" />    
/// <reference path="../angular-file-upload.min.js" /> 
var app;

(function () {
    app = angular.module("movieApp", []);
})();