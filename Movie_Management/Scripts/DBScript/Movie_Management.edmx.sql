
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/19/2019 14:39:27
-- Generated from EDMX file: C:\Users\Vignesh Prabhu\source\repos\Movie_Management\Movie_Management.DBRepository\Movie_Management.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Movie_Management];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_MapActorToShow_Actors]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MapActorToShow] DROP CONSTRAINT [FK_MapActorToShow_Actors];
GO
IF OBJECT_ID(N'[dbo].[FK_MapActorToShow_Shows]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MapActorToShow] DROP CONSTRAINT [FK_MapActorToShow_Shows];
GO
IF OBJECT_ID(N'[dbo].[FK_Shows_EntertainmentType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Shows] DROP CONSTRAINT [FK_Shows_EntertainmentType];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Actors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Actors];
GO
IF OBJECT_ID(N'[dbo].[EntertainmentType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EntertainmentType];
GO
IF OBJECT_ID(N'[dbo].[MapActorToShow]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MapActorToShow];
GO
IF OBJECT_ID(N'[dbo].[Shows]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Shows];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Actors'
CREATE TABLE [dbo].[Actors] (
    [ActorId] bigint IDENTITY(1,1) NOT NULL,
    [ActorName] nvarchar(50)  NOT NULL,
    [ActorSex] nvarchar(20)  NOT NULL,
    [ActorDOB] datetime  NOT NULL,
    [ActorBio] nvarchar(250)  NULL
);
GO

-- Creating table 'EntertainmentTypes'
CREATE TABLE [dbo].[EntertainmentTypes] (
    [EntertainmentTypeId] bigint IDENTITY(1,1) NOT NULL,
    [EntertainmentType1] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'MapActorToShows'
CREATE TABLE [dbo].[MapActorToShows] (
    [MapId] bigint IDENTITY(1,1) NOT NULL,
    [ShowId] bigint  NOT NULL,
    [ActorId] bigint  NOT NULL
);
GO

-- Creating table 'Shows'
CREATE TABLE [dbo].[Shows] (
    [ShowId] bigint IDENTITY(1,1) NOT NULL,
    [EntertainmentTypeId] bigint  NOT NULL,
    [ShowName] nvarchar(50)  NOT NULL,
    [ShowYearOfRelaease] nvarchar(50)  NOT NULL,
    [ShowPlot] nvarchar(250)  NOT NULL,
    [ImagePath] nvarchar(200)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ActorId] in table 'Actors'
ALTER TABLE [dbo].[Actors]
ADD CONSTRAINT [PK_Actors]
    PRIMARY KEY CLUSTERED ([ActorId] ASC);
GO

-- Creating primary key on [EntertainmentTypeId] in table 'EntertainmentTypes'
ALTER TABLE [dbo].[EntertainmentTypes]
ADD CONSTRAINT [PK_EntertainmentTypes]
    PRIMARY KEY CLUSTERED ([EntertainmentTypeId] ASC);
GO

-- Creating primary key on [MapId] in table 'MapActorToShows'
ALTER TABLE [dbo].[MapActorToShows]
ADD CONSTRAINT [PK_MapActorToShows]
    PRIMARY KEY CLUSTERED ([MapId] ASC);
GO

-- Creating primary key on [ShowId] in table 'Shows'
ALTER TABLE [dbo].[Shows]
ADD CONSTRAINT [PK_Shows]
    PRIMARY KEY CLUSTERED ([ShowId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ActorId] in table 'MapActorToShows'
ALTER TABLE [dbo].[MapActorToShows]
ADD CONSTRAINT [FK_MapActorToShow_Actors]
    FOREIGN KEY ([ActorId])
    REFERENCES [dbo].[Actors]
        ([ActorId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MapActorToShow_Actors'
CREATE INDEX [IX_FK_MapActorToShow_Actors]
ON [dbo].[MapActorToShows]
    ([ActorId]);
GO

-- Creating foreign key on [EntertainmentTypeId] in table 'Shows'
ALTER TABLE [dbo].[Shows]
ADD CONSTRAINT [FK_Shows_EntertainmentType]
    FOREIGN KEY ([EntertainmentTypeId])
    REFERENCES [dbo].[EntertainmentTypes]
        ([EntertainmentTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Shows_EntertainmentType'
CREATE INDEX [IX_FK_Shows_EntertainmentType]
ON [dbo].[Shows]
    ([EntertainmentTypeId]);
GO

-- Creating foreign key on [ShowId] in table 'MapActorToShows'
ALTER TABLE [dbo].[MapActorToShows]
ADD CONSTRAINT [FK_MapActorToShow_Shows]
    FOREIGN KEY ([ShowId])
    REFERENCES [dbo].[Shows]
        ([ShowId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MapActorToShow_Shows'
CREATE INDEX [IX_FK_MapActorToShow_Shows]
ON [dbo].[MapActorToShows]
    ([ShowId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------