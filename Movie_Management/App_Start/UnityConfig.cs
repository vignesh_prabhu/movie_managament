using Movie_Management.Service;
using Movie_Management.Service.ActorService;
using Movie_Management.Service.MovieService;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Movie_Management
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            container.RegisterType<IActorsService, ActorsService>();
            container.RegisterType<IMovieService, MovieService>();

        }
    }
}