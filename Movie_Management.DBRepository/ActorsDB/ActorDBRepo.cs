﻿using System.Collections.Generic;
using System.Linq;

namespace Movie_Management.DBRepository.ActorsDB
{
    
    public class ActorDbRepo
    {
        /// <summary>
        /// Get All Actors
        /// </summary>
        public List<Actor> GetActors()
        {
            using (var context = new Movie_ManagementEntities())
            {
                return context.Actors.ToList();
            }
        }

        /// <summary>
        /// Save Actor
        /// </summary>
        public List<Actor> SaveActor(Actor actor)
        {
            using (var context = new Movie_ManagementEntities())
            {
                context.Actors.Add(actor);
                context.SaveChanges();
                return context.Actors.OrderByDescending(x => x.ActorId).Take(1).ToList();
            }
        }

        /// <summary>
        /// Delete Actor
        /// </summary>
        /// <param name="ActorId"></param>
        public void DeleteActor(long ActorId)
        {
            using (var context = new Movie_ManagementEntities())
            {
                context.deleteActor(ActorId);
            }
        }

    }
}
