﻿using System.Collections.Generic;
using System.Linq;

namespace Movie_Management.DBRepository.MovieDB
{
    public class MovieDbRepo
    {
        #region SaveMovie

        public void SaveMovie(Show show)
        {
            using (var context = new Movie_ManagementEntities())
            {
                context.Shows.Add(show);
                context.SaveChangesAsync();
            }
        }

        #endregion

        #region GetLastestMovie
        /// <summary>
        /// Get Last Movie Record
        /// </summary>
        /// <param name="EntertainmentTypeId"></param>
        /// <returns></returns>
        public long GetLatestMovie(long EntertainmentTypeId)
        {
            using (var context = new Movie_ManagementEntities())
            {
                return context.Shows.Where(x => x.EntertainmentTypeId == EntertainmentTypeId).OrderByDescending(x => x.ShowId).Take(1).Select(x => x.ShowId).ToList().FirstOrDefault();
            }
        }

        #endregion

        #region MapActortoMovie
        /// <summary>
        /// Save Multiple Actor to the corresponding movie
        /// </summary>
        /// <param name="ShowId"></param>
        /// <param name="ActorId"></param>
        public void SaveMap(long ShowId,long ActorId)
        {
            using (var context = new Movie_ManagementEntities())
            {
                MapActorToShow map = new MapActorToShow() { MapId = 0 ,ShowId = ShowId, ActorId = ActorId };
                context.MapActorToShows.Add(map);
                context.SaveChanges();
            }
        }

        #endregion

        #region GetAllMovie
        /// <summary>
        /// Get List of All Movies
        /// </summary>
        /// <returns></returns>
        public List<GetAllMovies_Result> GetAllMovies()
        {
            using (var context = new Movie_ManagementEntities())
            {
                var a = context.GetAllMovies();

                return a.ToList();
            }
        }

        #endregion

        #region Get Movie With Respect to id
        /// <returns></returns>
        public List<Show> EditShow()
        {
            using (var context = new Movie_ManagementEntities())
            {
                return context.Shows.ToList();
                
            }
        }

        public List<long> EditShowtoActorId (long id)
        {
            using (var context = new Movie_ManagementEntities())
            {
                return context.MapActorToShows.Where(s => s.ShowId == id).Select(a => a.ActorId).ToList();
            }
        }

        public List<Actor> GetActor(long id)
        {
            using (var context = new Movie_ManagementEntities())
            {
                return context.Actors.Where(a => a.ActorId == id).ToList();
            }
        }

        public void EditMovie(Show show)
        {
            using (var context = new Movie_ManagementEntities())
            {
                context.Entry(show).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteMovie(long MovieId)
        {
            using (var context = new Movie_ManagementEntities())
            {
                context.deleteMovie(MovieId);
            }
        }


        #endregion

    }
}
