﻿CREATE PROCEDURE [dbo].[deleteActor]
(
	@ActorId bigint
)
AS

BEGIN

DELETE FROM [dbo].[Actors] WHERE ActorId = @ActorId

END