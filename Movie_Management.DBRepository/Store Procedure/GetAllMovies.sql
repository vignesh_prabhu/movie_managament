﻿CREATE PROCEDURE [dbo].[GetAllMovies]

AS

BEGIN

SELECT C.ShowId,C.ShowName,C.ShowYearOfRelaease,C.ShowPlot,C.ImagePath, X.[Cast] FROM     
dbo.[Shows] C  
  
CROSS APPLY    
(    
SELECT STUFF    
            (    
                (    
                    SELECT ',' + P.ActorName FROM dbo.[Actors] AS P    
                    INNER JOIN dbo.[MapActorToShow] AS CP ON P.ActorId=CP.ActorId     
                    WHERE CP.ShowId=C.ShowId    
                    FOR XML PATH('')    
                )    
                    
            ,1,1,'') as [Cast]    
) as X 
WHERE C.EntertainmentTypeId =1
END
