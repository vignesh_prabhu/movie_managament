﻿CREATE PROCEDURE [dbo].[GetShowdetailWithShowId]
(
	@ShowId bigint
)
AS

BEGIN

SELECT S.ShowId, S.ShowName, S.ShowYearOfRelaease, S.ShowPlot, S.ImagePath, M.ActorId 
FROM Shows S
JOIN MapActorToShow M on S.ShowId = M.ShowId
Where s.ShowId = @ShowId

END