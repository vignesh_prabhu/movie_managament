﻿using Movie_Management.Business.MovieBL;
using Movie_Management.Domain;
using System;
using System.Collections.Generic;

namespace Movie_Management.Service.MovieService
{
    public class MovieService : IMovieService
    {
        private readonly MovieBl _movieBl;

        public MovieService()
        {
            _movieBl = new MovieBl();
        }

        public bool SaveMovie(string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId,long[] Cast)
        {
            try
            {
                _movieBl.SaveMovie(ShowName, ShowYearOfRelease, ShowPlot, ImagePath, EntertainmentId,Cast);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllMovies> GetAllMovie()
        {
            return _movieBl.GetAllMovies();
        }

        public GetShowWithId EditMovie(long id)
        {
            return _movieBl.EditMovie(id);
        }

        public bool EditMovieDetails(long showId, string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId, long[] Cast)
        {
            try
            {
                _movieBl.EditMovie(showId, ShowName, ShowYearOfRelease, ShowPlot, ImagePath, EntertainmentId, Cast);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
