﻿using Movie_Management.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Service.MovieService
{
    public interface IMovieService
    {
        bool SaveMovie(string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId,long[] Cast);
        List<GetAllMovies> GetAllMovie();
        GetShowWithId EditMovie(long id);
        bool EditMovieDetails(long showId, string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId, long[] Cast);
    }
}
