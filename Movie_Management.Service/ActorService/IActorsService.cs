﻿using Movie_Management.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Service.ActorService
{
    public interface IActorsService {

        List<Actors> GetActors();
        List<Actors> SaveActor(long? ActorId, string ActorName, string ActorSex, DateTime ActorDob, string ActorBio);
        bool DeleteActor(long ActorId);
        Actors EditActor(long id);
    }
}
