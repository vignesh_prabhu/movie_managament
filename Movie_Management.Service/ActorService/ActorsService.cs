﻿using System;
using System.Collections.Generic;
using Movie_Management.Business;
using Movie_Management.Business.ActorsBL;
using Movie_Management.Domain;

namespace Movie_Management.Service.ActorService
{
    public class ActorsService : IActorsService
    {
        private readonly ActorBl _actorBl;

        public ActorsService()
        {
            _actorBl = new ActorBl();
        }

        public List<Actors> GetActors()
        {
            return _actorBl.GetActors();
        }

        public List<Actors> SaveActor(long? ActorId, string ActorName, string ActorSex, DateTime ActorDob, string ActorBio)
        {
             var actor = _actorBl.SaveActor(ActorId, ActorName, ActorSex, ActorDob, ActorBio);
             return actor;
            
        }

        public bool DeleteActor(long ActorId)
        {
            try
            {
                _actorBl.DeleteActor(ActorId);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        } 

        public Actors EditActor(long id)
        {
            return _actorBl.EditActor(id);
        }
    }
}
