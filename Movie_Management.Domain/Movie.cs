﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Domain
{
    public class Movie
    {
        public long ShowId { get; set; }
        public long EntertainmentId { get; set; }
        public string ShowName { get; set; }
        public string ShowYearOfRelease { get; set; }
        public string ShowPlot { get; set; }
        public string ImagePath { get; set; }
    }
}
