﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Domain
{
    public class MapActorToShow
    {
        public long MapId { get; set; }
        public long ShowId { get; set; }
        public long ActorId { get; set; }
    }
}
