﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Domain
{
    public class EntertainmentType
    {
        public long EntertainmentTypeId { get; set; }
        public long EntertainmentTypeName { get; set; }
    }
}
