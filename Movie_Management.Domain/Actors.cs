﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Domain
{
    public class Actors
    {
        public long ActorId { get; set; }
        public string ActorName { get; set; }
        public string ActorSex { get; set; }
        public DateTimeOffset ActorDob { get; set; }
        public string ActorBio { get; set; }
    }
}
