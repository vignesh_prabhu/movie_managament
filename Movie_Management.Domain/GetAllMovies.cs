﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_Management.Domain
{
    public class GetAllMovies
    {
        public long ShowId { get; set; }
        public string ShowName { get; set; }
        public string ShowYearOfRelaease { get; set; }
        public string ShowPlot { get; set; }
        public string ImagePath { get; set; }
        public string Cast { get; set; }
    }
}
