﻿using System;
using System.Collections.Generic;
using System.Linq;
using Movie_Management.DBRepository;
using Movie_Management.DBRepository.ActorsDB;
using Movie_Management.Domain;

namespace Movie_Management.Business.ActorsBL
{
    public class ActorBl
    {
        private readonly ActorDbRepo _dbInstanceActorsRepo;

        public ActorBl()
        {
            _dbInstanceActorsRepo = new ActorDbRepo();
        }

        #region GetActors

            public List<Actors> GetActors()
            {
                var actorName = _dbInstanceActorsRepo.GetActors();

                List<Actors> actors = new List<Actors>();

                if (actorName != null && actorName.Any())
                {
                    actorName.ForEach(Entities => {
                        actors
                        .Add(new Actors {
                            ActorName = Entities.ActorName,
                            ActorBio = Entities.ActorBio,
                            ActorDob = Entities.ActorDOB,
                            ActorId = Entities.ActorId,
                            ActorSex = Entities.ActorSex
                        });
                    });
                }
                return actors;
            }

        #endregion

        #region SaveActor

        public List<Actors> SaveActor(long? ActorId, string ActorName, string ActorSex, DateTime ActorDob, string ActorBio)
        {
            var actor = _dbInstanceActorsRepo.SaveActor(new Actor
            {
                ActorBio = ActorBio,
                ActorDOB = ActorDob,
                ActorId = ActorId ?? 0,
                ActorName = ActorName,
                ActorSex = ActorSex,
            });

            List<Actors> actors = new List<Actors>();

            if (actor != null && actor.Any())
            {
                actor.ForEach(Entities => {
                    actors
                    .Add(new Actors
                    {
                        ActorName = Entities.ActorName,
                        ActorBio = Entities.ActorBio,
                        ActorDob = Entities.ActorDOB,
                        ActorId = Entities.ActorId,
                        ActorSex = Entities.ActorSex
                    });
                });
            }
            return actors;
        }

        #endregion

        #region DeleteActorDetail

        public void DeleteActor(long ActorId)
        {
            _dbInstanceActorsRepo.DeleteActor(ActorId);
        }

        #endregion

        #region

        public Actors EditActor(long id)
        {
            var details = _dbInstanceActorsRepo.GetActors();
            List<Actors> editActor = new List<Actors>();

            if (details != null && details.Any())
            {
                details.ForEach(Entities => {
                    editActor.Add(new Actors
                    {
                        ActorBio = Entities.ActorBio,
                        ActorDob = Entities.ActorDOB,
                        ActorName = Entities.ActorName,
                        ActorId = Entities.ActorId,
                        ActorSex = Entities.ActorSex
                    });
                });
            }

            var actorId = editActor.FirstOrDefault(e => e.ActorId.Equals(id));
            return actorId;
        }

        #endregion
    }
}
