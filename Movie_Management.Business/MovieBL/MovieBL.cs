﻿using Movie_Management.DBRepository.MovieDB;
using Movie_Management.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Movie_Management.Business.MovieBL
{
    public class MovieBl
    {
        private readonly MovieDbRepo _dbInstanceMovieRepo;

        public MovieBl()
        {
            _dbInstanceMovieRepo = new MovieDbRepo();
        }

        public void SaveMovie(string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath,  long EntertainmentId,long[] Cast)
        {
            if(Cast != null)
            {
                _dbInstanceMovieRepo.SaveMovie(new DBRepository.Show
                {
                    ShowId = 0,
                    EntertainmentTypeId = EntertainmentId,
                    ImagePath = ImagePath,
                    ShowName = ShowName,
                    ShowPlot = ShowPlot,
                    ShowYearOfRelaease = ShowYearOfRelease
                });

                var latestMovieId = _dbInstanceMovieRepo.GetLatestMovie(EntertainmentId);

                foreach (var actorId in Cast)
                {
                    _dbInstanceMovieRepo.SaveMap(latestMovieId, actorId);
                }
            }
            
        }

        public List<GetAllMovies> GetAllMovies()
        {
            var listOfMovies = _dbInstanceMovieRepo.GetAllMovies();

            List<GetAllMovies> ret = new List<GetAllMovies>();

            if (listOfMovies != null && listOfMovies.Any())
            {
                listOfMovies.ForEach(Entities => {
                    ret
                    .Add(new GetAllMovies
                    {
                        ShowId = Entities.ShowId,
                        Cast = Entities.Cast,
                        ImagePath = Entities.ImagePath,
                        ShowName = Entities.ShowName,
                        ShowPlot = Entities.ShowPlot,
                        ShowYearOfRelaease = Entities.ShowYearOfRelaease
                    });
                });
            }
            return ret;
        }

        public GetShowWithId EditMovie(long id)
        {
            var actorId = _dbInstanceMovieRepo.EditShowtoActorId(id).ToArray();

            var movie = _dbInstanceMovieRepo.EditShow();

            List<GetShowWithId> ret = new List<GetShowWithId>();

            if (movie != null && movie.Any())
            {
                movie.ForEach(Entities => {
                    ret
                    .Add(new GetShowWithId
                    {
                        ShowId = Entities.ShowId,
                        Actors = actorId,
                        ImagePath = Entities.ImagePath,
                        ShowName = Entities.ShowName,
                        ShowPlot = Entities.ShowPlot,
                        ShowYearOfRelaease = Entities.ShowYearOfRelaease
                    });
                });
            }
            var editShow = ret.FirstOrDefault(e => e.ShowId == id);
            return editShow;
        }

        public void EditMovie(long showId, string ShowName, string ShowYearOfRelease, string ShowPlot, string ImagePath, long EntertainmentId, long[] Cast)
        {
            if (Cast != null)
            {
                _dbInstanceMovieRepo.EditMovie(new DBRepository.Show
                {
                    ShowId = showId,
                    EntertainmentTypeId = EntertainmentId,
                    ImagePath = ImagePath,
                    ShowName = ShowName,
                    ShowPlot = ShowPlot,
                    ShowYearOfRelaease = ShowYearOfRelease
                });

            }

            _dbInstanceMovieRepo.DeleteMovie(showId);

            if (Cast == null) return;
            foreach (var actorId in Cast)
            {
                _dbInstanceMovieRepo.SaveMap(showId, actorId);
            }
        }
    }
}
