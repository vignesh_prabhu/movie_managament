#Instructions to Run the application 

1) Pull the Project repository from bitbucket using "git clone https://Movie_Management@bitbucket.org/Movie_Management/movie_managament.git"
2) Navigate to the cloned Repository folder 
3) Open "Movie_Management.sln" using Visual Studio
4) Expand "Movie_Management" under Solution Explorer , open "Web.config" file , Press Ctrl+F and search for "data source=LAPTOP-UR7F1IDK\SQLEXPRESS_2017N;initial catalog=Movie_Management;user id=sa;password=root;"
5) Replace ID & Password with Credentials of SQL Server DB , also the Data Source value with that of SQL Server Name

#Path of Screen Shots 
 -> Open Movie_Management -> Open Contents Folder -> Screen Shots

#Path of DB Script
 -> Open Movie_Management -> Open Scripts Folder -> Open DBScripts -> Open Movie_Management.edmx.sql Copy the content in DBScript and Execute in SQL Server 
 
#Path of Stored Procedures
-> After Completing Creating the DataBase
-> Go to Movie_Management.DBRepository -> Open Stored Procedure folder -> Execute the scripts in SQL server

#Frameworks/Libraries Used 
 -> Front-End : Bootstrap 3 , AngularJS , jQuery
 -> Back-End  : ASP.NET MVC5 , C#

#Version Of Software
 -> Visual Studio 2017
 -> SQL SERVER 2017