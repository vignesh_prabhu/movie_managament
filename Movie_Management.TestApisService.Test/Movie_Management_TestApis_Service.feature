﻿Feature: Movie_Management_TestApis_Service
	CURD operation on movie service

@mytag
Scenario: Get a movie detail by id
	Given movie id 23
	When movie is fetched based on movie id
	Then movie should look like 
	| ShowId | ShowName | ShowYearOfRelaease | ShowPlot                                                                                                                                                                                              | ImagePath  |
	| 23     | Avatar   | 2009               | Jake, a paraplegic marine, replaces his brother on the Na'vi inhabited Pandora for a corporate mission. He is accepted by the natives as one of their own but he must decide where his loyalties lie. | Avatar.png |
	Then actors in movie should look like
	| Actor  |
	|  55    |
	|  56    |
	|  57    |

@mytag
Scenario: Save movie detail
	Given movie data is
	| ShowName | ShowYearOfRelaease | ShowPlot | ImagePath | Actor |
	| Test     | 2019               | Testxyz  | Test.jpg  | 55,56 |
	Then save movie in database 
	And return result