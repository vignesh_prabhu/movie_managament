﻿using System;
using System.Collections.Generic;
using System.Linq;
using Movie_Management.Domain;
using Movie_Management.Service.MovieService;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Movie_Management.TestApisService.Test
{
    [Binding]
    public class MovieManagementTestApisServiceSteps
    {
        private readonly IMovieService _movieService = new MovieService();


        #region Scenario: Get movie by id

        private long _movieId;
        private GetShowWithId _movie;

        [Given(@"movie id (.*)")]
        public void GivenMovieId(long id)
        {
            _movieId = id;
        }

        [When(@"movie is fetched based on movie id")]
        public void WhenMovieIsFetchedBasedOnMovieId()
        {
            _movie = _movieService.EditMovie(_movieId);
        }

        [Then(@"movie should look like")]
        public void ThenMovieShouldLookLike(Table table)
        {
            table.CompareToInstance(_movie);
        }

        [Then(@"actors in movie should look like")]
        public void ThenActorsInMovieShouldLookLike(Table table)
        {
            table.CompareToSet(_movie.Actors.Select(x => new { Actor = x }));
        }

        #endregion

        #region Scenario: Save movie in database

        public class MovieDetail
        {
            public string ShowName { get; set; }
            public string ShowYearOfRelaease { get; set; }
            public string ShowPlot { get; set; }
            public string ImagePath { get; set; }
            public long EntertainmentId { get; set; }
            public long[] Actor { get; set; }
        }

        private bool _result;
        private MovieDetail _movieDetail;

        [Given(@"movie data is")]
        [Obsolete]
        public void GivenMovieDataIs(Table table)
        {
            _movieDetail = table.CreateInstance<MovieDetail>();
        }

        [Then(@"save movie in database")]
        public void ThenSaveMovieInDatabase()
        {
            _result = _movieService.SaveMovie(
                _movieDetail.ShowName, 
                _movieDetail.ShowYearOfRelaease,
                _movieDetail.ShowPlot,
                _movieDetail.ImagePath,
                1,
                _movieDetail.Actor
                );
        }

        [Then(@"return result")]
        public void ThenReturnResult()
        {
            Assert.IsTrue(_result);
        }

        #endregion

    }
}